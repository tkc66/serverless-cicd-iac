#--------------------------
# 設定
#--------------------------

# exit status が 0 以外でも終了させない
set +e

# 更新チェック対象パス
check_path=$WORKSPACE/pyproject.toml

#--------------------------
# 実行
#--------------------------

# workspaceに移動
cd $WORKSPACE

# branch取得
branch=`git rev-parse --abbrev-ref HEAD`

# 現在のハッシュ値取得
old=`git log -n 1 --format=format:'%H'`

# pull
git pull origin $branch

# 更新後のハッシュ値取得
new=`git log -n 1 --format=format:'%H'`

# 該当パスに変更があるかチェック
# ハッシュ値間のコミットのファイルに対象パスでgrepをかける
git log $old..$new --format=format:'' --name-status | grep -e "^[ADM]\s\+$check_path"


if [ $? = 0 ]; then
    poetry export -f requirements.txt > src/requirements.txt --without-hashes
    echo " Success to deploy lambda layer"
    else
    echo "Skip to deploy lambda layer"
fi