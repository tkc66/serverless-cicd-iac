"""
日時の処理に関する共通関数。
"""

from datetime import date, datetime, time, timezone


def utcnow():
    """
    UTCの現在日時を返す。

    Return:
        datetime: UTCの現在日時
    """
    return datetime.now(tz=timezone.utc)


def format_timestamp(obj):
    """
    日時のデータをISO8601フォーマットに変換する。
    datetime, date, time 以外は変換しない。

    Example:
        datetime -> '2021-03-12T13:50:12.123Z'
        date -> '2021-03-12'
        time -> '13:50:12.123'
    """
    if obj is None:
        return obj
    elif isinstance(obj, date) or isinstance(obj, time) or isinstance(obj, datetime):
        return _format_timestamp(obj)
    elif isinstance(obj, list):
        return [format_timestamp(o) for o in obj]
    elif isinstance(obj, dict):
        return {key: format_timestamp(value) for key, value in obj.items()}

    return obj


def timestamp_from(str: str):
    """
    ISO8601フォーマットの文字列をdatetimeのオブジェクトに変換する。
    """
    if str[-1:] == 'Z':
        iso_str = str.replace('Z', '+00:00')
        tz = timezone.utc
    else:
        iso_str = str
        tz = None

    dt = datetime.fromisoformat(iso_str)
    if tz is not None:
        dt = dt.astimezone(tz)

    return dt


def asutc(dt: datetime):
    """
    タイムゾーンをUTCに変換する。
    """
    dt_utc = dt.astimezone(tz=timezone.utc)
    return dt_utc


def _format_timestamp(timestamp):
    """
    日時のデータをISO8601フォーマットに変換する。
    """
    if isinstance(timestamp, datetime):
        timestamp = timestamp.astimezone(tz=timezone.utc)
        return timestamp.isoformat()[:-9] + 'Z'
    elif isinstance(timestamp, date):
        return timestamp.isoformat()
    elif isinstance(timestamp, time):
        return timestamp.isoformat()[:-3]
