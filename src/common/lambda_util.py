"""
AWS Lambda に関する共通関数。
"""

import dataclasses
import decimal
import json
from dataclasses import dataclass
from datetime import date, datetime, time

from .timestamp_util import format_timestamp


@dataclass
class DomainEntity():
    """
    ドメインエンティティの基底クラス。
    """

    def to_dict(self, remove_none_field: bool = False, as_primitive: bool = False):
        data = dataclasses.asdict(self)
        if remove_none_field:
            data = _remove_none(data)

        if as_primitive:
            dict_data = convert_to_primitive(data)
        else:
            dict_data = data
        return dict_data

    def to_persistence(self, remove_none_field: bool = False):
        data = dataclasses.asdict(self)
        if remove_none_field:
            data = _remove_none(data)

        persistence_data = convert_to_persistence(data)
        return persistence_data

    def update(self, new_entity):
        for key, value in new_entity.items():
            if hasattr(self, key):
                setattr(self, key, value)


class JsonEncoder(json.JSONEncoder):
    """
    オブジェクトのJSON変換時のエンコーダー。
    ・数値（Decimal）は、int or float に変換される。
    ・日時は、ISO8601のUTC形式の文字列に変換される。
    """

    def default(self, o):
        """
        変換処理を行う。
        """

        if isinstance(o, decimal.Decimal):
            return _decimal_to_num(o)
        elif isinstance(o, date) or isinstance(o, time) or isinstance(o, datetime):
            return _convert_timestamp(o)

        return super(JsonEncoder, self).default(o)


def convert_to_primitive(obj):
    """
    指定されたオブジェクトをプリミティブ型の値に変換する。
    ・数値（Decimal）は、int or float に変換される。
    ・日時は、ISO8601のUTC形式の文字列に変換される。
    """

    if isinstance(obj, decimal.Decimal):
        return _decimal_to_num(obj)
    elif isinstance(obj, date) or isinstance(obj, time) or isinstance(obj, datetime):
        return _convert_timestamp(obj)
    elif type(obj) == dict:
        return {key: convert_to_primitive(value) for key, value in obj.items()}
    elif type(obj) == list:
        return [convert_to_primitive(value) for value in obj]
    elif dataclasses.is_dataclass(obj):
        data = dataclasses.asdict(obj)
        return convert_to_primitive(data)
    else:
        return obj


def convert_to_persistence(obj):
    """
    指定されたオブジェクトを永続化用のデータに変換する（DynamoDBへの保存で利用）。
    ・数値（int or float）は、Decimal に変換される。
    ・日時は、ISO8601のUTC形式の文字列に変換される。
    """

    if type(obj) in {int, float}:
        return _num_to_decimal(obj)
    elif isinstance(obj, date) or isinstance(obj, time) or isinstance(obj, datetime):
        return _convert_timestamp(obj)
    elif type(obj) == dict:
        return {key: convert_to_persistence(value) for key, value in obj.items()}
    elif type(obj) == list:
        return [convert_to_persistence(value) for value in obj]
    elif dataclasses.is_dataclass(obj):
        data = dataclasses.asdict(obj)
        return convert_to_persistence(data)
    else:
        return obj


# ----- Lambda request/response


def response(event, body, status_code='200'):
    """
    Lambdaプロキシ統合の正常レスポンスを生成する。

    Args:
        event (dict): Lambda関数のevent
        body (dict): レスポンスの内容
        status_code (str or int): HTTPステータスコード（デフォルト:'200'）

    Returns:
        dict(json): レスポンス
    """

    dict_data = convert_to_primitive(body)
    dict_data = _remove_none(dict_data)

    allow_origin = None
    if event.get('headers'):
        allow_origin = event.get('headers').get('origin')

    return {
        'statusCode': str(status_code),
        'body': json.dumps(dict_data, cls=JsonEncoder),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': allow_origin
        }
    }


def error_response(event, status_code, error_code, message, params=None):
    """
    Lambdaプロキシ統合のエラーレスポンスを生成する。

    Args:
        event (dict): Lambda関数のevent
        status_code (str or int): HTTPステータスコード（デフォルト:'200'）
        error_code (str): エラーコード
        message (str): エラーメッセージ
        params (dict): 画面側メッセージ出力用の変数値等（必要な場合だけ指定）

    Returns:
        dict(json): エラーレスポンス
    """
    allow_origin = None
    if event.get('headers'):
        allow_origin = event.get('headers').get('origin')

    body = {}
    if error_code:
        body = {
            'error': {
                'code': error_code,
                'message': message,
            }
        }

        if params:
            body['error']['params'] = params

    return {
        'statusCode': str(status_code),
        'body': json.dumps(body, cls=JsonEncoder),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': allow_origin
        }
    }


def get_body(event):
    """
    Lambdaプロキシ統合のリクエストからbodyを取得する。

    Args:
        event (dict): Lambda関数のevent

    Returns:
        dict(json): リクエストのbody
    """
    body_str = event.get('body')
    if body_str is None:
        return None
    body = json.loads(body_str)
    return body


def get_path_param(event, key):
    """
    Lambdaプロキシ統合のリクエストからパスパラメータを取得する。

    Args:
        event (dict): Lambda関数のevent

    Returns:
        str : リクエストのパスパラメータ
    """
    path_param_list = event.get('pathParameters', {})
    path_param = path_param_list.get(key)
    return path_param


def get_query_param(event, key):
    """
    Lambdaプロキシ統合のリクエストからクエリストリングパラメータを取得する。

    Args:
        event (dict): Lambda関数のevent

    Returns:
        dict : リクエストのクエリストリングパラメータ
    """
    query_param_list = event.get('queryStringParameters', {})
    query_param = query_param_list.get(key)
    return query_param


# ----- Inner functions


def _num_to_decimal(obj):
    """
    数値をDecimal型へ変換する。数値以外の場合はエラーとなる。
    DynamoDBへの保存用に必要となる。
    """
    return decimal.Decimal(str(obj))


def _decimal_to_num(obj):
    """
    Decimal型を数値へ変換する。数値以外の場合はエラーとなる。
    DynamoDBからのデータ取得用に必要となる。
    """
    if obj % 1 != 0:
        return float(obj)
    else:
        return int(obj)


def _convert_timestamp(obj):
    """
    日時のオブジェクトを適切な文字列に変換する。
    """
    return format_timestamp(obj)


def _remove_none(dict_data):
    """
    JSON形式に変換するために、dictのオブジェクトからNoneの要素を削除する。
    """
    if isinstance(dict_data, list):
        list_data = []
        for item in dict_data:
            list_data.append(_remove_none(item))
        return list_data
    elif not isinstance(dict_data, dict):
        return dict_data

    for k, v in list(dict_data.items()):
        if v is None:
            dict_data.pop(k, None)
    return dict_data
