import uuid

import boto3
from aws_lambda_powertools import Logger
from common.timestamp_util import format_timestamp, utcnow

from .todo import Todo

logger = Logger(child=True)


class TodoService:
    """
    TODOの処理を行うクラス。
    """

    def __init__(self):
        self.__dynamodb = boto3.resource('dynamodb')

    def find_todo_list(self):
        """
        TODOの一覧を取得する。

        Returns:
            登録されているTODOの一覧
        """
        todo_table = self.__dynamodb.Table('todo')
        # scanの利用は注意すること
        todo_response = todo_table.scan(Limit=100)
        todo_item_list = todo_response.get('Items')

        todo_list = []
        for todo_item in todo_item_list:
            todo = Todo(**todo_item)
            todo_list.append(todo)

        return todo_list

    def find_todo(self, todo_id: str):
        """
        TODOを取得する。

        Args:
            todo_id: TODOのID

        Returns:
            指定されたTODOの内容
        """
        todo_table = self.__dynamodb.Table('todo')
        todo_response = todo_table.get_item(Key={'todo_id': todo_id})
        todo_item = todo_response.get('Item')

        if todo_item is None:
            todo = None
        else:
            todo = Todo(**todo_item)

        return todo

    def add_todo(self, item: dict):
        """
        TODOを登録する。

        Args:
            item: TODOの内容

        Returns:
            登録されたTODOの内容
        """
        todo_table = self.__dynamodb.Table('todo')

        todo_item = item.copy()
        todo_item['todo_id'] = str(uuid.uuid4())
        now_str = format_timestamp(utcnow())

        todo = Todo(**todo_item)
        todo.updated_timestamp = now_str

        logger.info('Add new todo. : todo=%s', todo)

        todo_table.put_item(Item=todo.to_persistence())

        return todo

    def update_todo(self, item: dict):
        """
        TODOを更新する。

        Args:
            item: TODOの内容

        Returns:
            更新されたTODOの内容
        """
        todo_table = self.__dynamodb.Table('todo')

        todo_id = item['todo_id']
        now = utcnow()

        todo = self.find_todo(todo_id)
        if todo is None:
            raise ValueError(f'Invalid todo_id. : {todo_id}')

        todo.update(item)
        todo.updated_timestamp = now

        logger.info('Update todo. : todo=%s', todo)

        todo_table.put_item(Item=todo.to_persistence())

        return todo

    def delete_todo(self, todo_id: str):
        """
        TODOを削除する。

        Args:
            todo_id: TODOのID

        Returns:
            削除に成功した場合はTrue、削除対象が存在しなかった場合はFalse
        """
        todo_table = self.__dynamodb.Table('todo')
        todo_response = todo_table.delete_item(Key={'todo_id': todo_id}, ReturnValues='ALL_OLD')
        todo_item = todo_response.get('Attributes', {})

        if todo_item.get('todo_id') is not None:
            deleted = True
        else:
            deleted = False

        return deleted
