from dataclasses import dataclass
from datetime import datetime

from common.lambda_util import DomainEntity
from common.timestamp_util import timestamp_from, asutc

# イミュータブルなオブジェクトにする場合は、以下のようにする。
# @dataclass(frozen=True)


@dataclass
class Todo(DomainEntity):
    todo_id: str
    todo: str
    updated_timestamp: datetime = None

    def __post_init__(self):
        # タイムスタンプが文字列の場合にdatetimeへ変換
        if type(self.updated_timestamp) == str:
            self.updated_timestamp = asutc(timestamp_from(self.updated_timestamp))
