from aws_lambda_powertools import Logger, Tracer
from common.lambda_util import get_path_param, get_body, response

from domain.todo_service import TodoService

logger = Logger()
tracer = Tracer(patch_modules=['boto3', 'requests'])


@logger.inject_lambda_context()
def find_todo_list_handler(event, context):
    """
    TODOの一覧を取得する。
    """
    todo_service = TodoService()
    todo_list = todo_service.find_todo_list()

    return response(event, todo_list)


@logger.inject_lambda_context()
def find_todo_handler(event, context):
    """
    TODOを取得する。
    """
    todo_id = get_path_param(event, 'todo_id')

    todo_service = TodoService()
    todo = todo_service.find_todo(todo_id)

    return response(event, todo)


@logger.inject_lambda_context()
def add_todo_handler(event, context):
    """
    TODOを登録する。
    """
    item = get_body(event)

    todo_service = TodoService()
    todo = todo_service.add_todo(item)

    return response(event, todo)


@logger.inject_lambda_context()
def update_todo_handler(event, context):
    """
    TODOを更新する。
    """
    item = get_body(event)

    todo_service = TodoService()
    todo = todo_service.update_todo(item)

    return response(event, todo)


@logger.inject_lambda_context()
def delete_todo_handler(event, context):
    """
    TODOを削除する。
    """
    todo_id = get_path_param(event, 'todo_id')

    todo_service = TodoService()
    result = todo_service.delete_todo(todo_id)

    return response(event, result)
