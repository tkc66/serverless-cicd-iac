import boto3

from botocore.exceptions import ClientError


class AuthenticationService:
    """
    Class to authenticate the received user.
    """

    def __init__(self, cognito_user_pool_id=None, cognito_client_id=None, auth_flow='ADMIN_USER_PASSWORD_AUTH'):
        self.__cognito_user_pool_id = cognito_user_pool_id
        self.__cognito_client_id = cognito_client_id
        self.__auth_flow = auth_flow

    def authenticate(self, email, password):
        """
        Get login token related to the inputted email and password.

        :return: token
        """
        client = boto3.session.Session().client('cognito-idp')
        try:
            auth_response = client.admin_initiate_auth(
                UserPoolId=self.__cognito_user_pool_id,
                ClientId=self.__cognito_client_id,
                AuthFlow=self.__auth_flow,
                AuthParameters={
                    'USERNAME': email,
                    'PASSWORD': password
                }
            )
        except ClientError as err:
            raise LoginFailureError('Login failed') from err

        id_token = auth_response.get('AuthenticationResult', {}).get('IdToken')
        if id_token:
            return id_token
        else:
            raise LoginFailureError('Login failed')


class LoginFailureError(Exception):
    """
    Exception for when the login returns empty.
    """

    def __init__(self, message):
        """
        Initialization
        """
        self.message = message
