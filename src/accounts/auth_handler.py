import os

from aws_lambda_powertools import Logger, Tracer
from common.lambda_util import error_response, get_body, response

from domain.authentication_service import (AuthenticationService,
                                           LoginFailureError)

logger = Logger()
tracer = Tracer(patch_modules=['boto3', 'requests'])

COGNITO_USER_POOL_ID = os.environ.get('COGNITO_USER_POOL_ID', '')
COGNITO_CLIENT_ID = os.environ.get('COGNITO_CLIENT_ID', '')
COGNITO_AUTH_FLOW = 'ADMIN_USER_PASSWORD_AUTH'


@logger.inject_lambda_context()
def authenticate_handler(event, context):
    """
    メールアドレス／パスワードでのCognito認証を行う。
    """
    request = get_body(event)
    email = request.get('email')
    password = request.get('password')

    if email is None or password is None:
        logger.error('Failure to authenticate. : email=%s, password=%s', email, password)
        message = 'Invalid parameters: email or password'
        error_code = 'accounts.auth.authenticate.invalid_parameters'
        return error_response(event, 400, error_code=error_code, message=message)

    auth = AuthenticationService(cognito_user_pool_id=COGNITO_USER_POOL_ID,
                                 cognito_client_id=COGNITO_CLIENT_ID,
                                 auth_flow=COGNITO_AUTH_FLOW)
    try:
        token = auth.authenticate(email, password)
    except LoginFailureError as err:
        logger.exception('Failure to authenticate. : email=%s', email)
        error_code = 'accounts.auth.authenticate.login_failure'
        return error_response(event, 401, error_code=error_code, message=err.message)
    except Exception as err:
        logger.exception('Failure to authenticate. : email=%s', email)
        error_code = 'accounts.auth.authenticate.login_failure'
        return error_response(event, 500, error_code=error_code, message=err.message)

    return response(event, {'token': token})
