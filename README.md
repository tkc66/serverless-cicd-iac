# serverless-example-python-vscode

AWS Serverless アプリを VSCode を利用して開発する場合のブランクプロジェクト

## Develop Envirionment

### 事前準備

#### 必要モジュールの確認

- npm >= 6.14.5
  - `> npm --version`
- python >= 3.8.0
  - `> python --version`
- aws cli >= 2.0.0
  - `> aws --version`

Proxy 環境下では、事前に環境変数を設定をしておく。

```
> set HTTP_PROXY=http://192.168.99.200:8080
> set HTTPS_PROXY=http://192.168.99.200:8080
```

#### AWS プロファイルの作成

ここでは仮に「example」という名前でプロファイルを作成する。
プロジェクトで適切なプロファイル名／リージョンで作成すること。

```
> aws configure --profile example
AWS Access Key ID [None]: {your-access-key}
AWS Secret Access Key [None]: {your-secret-access-key}
Default region name [None]: us-west-2
Default output format [None]: json
```

### Python

- Pyenv(pyenv-win) Install
  - https://pyenv-win.github.io/pyenv-win/#installation
- Poetry Install
  - https://python-poetry.org/docs/#introduction

Pyenv／Poetry が既にインストールされている場合は、最新版に更新する。

```
> pyenv update
> poetry self update
```

最初に、プロジェクトのルートディレクトリで、Python の環境構築を行う。

```
> cd serverless-example-python-vscode
```

Pyenv を利用して、プロジェクトで利用する Python を指定する。

```
> pyenv local 3.8.X
> pyenv rehash
```

Poetry の設定として、プロジェクト内に ".venv" ディレクトリが作成されるようにする。

```
> poetry config virtualenvs.in-project true
```

Python 仮想環境にて、Poery を利用して関連モジュールをインストールする。

```
> poetry install
```

上記を行うと、serverless-example-python-vscode 配下に「.venv」という Python 仮想環境が作成される。
この Python 仮想環境を利用して作業をする場合は、以下のようにして仮想環境をアクティブ化する。

```
> .venv\Scripts\activate.bat

(.venv) > python --version
Python 3.8.X（仮想環境で指定したPythonのバージョン）
```

### Serverless Framework

Serverless Framework をインストールする。
既にインストールされている場合は、最新版にアップデートする。

```
> npm install -g serverless
```

```
> npm update -g serverless
```

関連モジュールをインストールする。

```
> cd serverless-example-python-vscode
> npm install
```

#### VSCode

- 以下のサイトの保存時の自動フォーマッタを ON にする。
  - https://qiita.com/mitashun/items/e2f118a9ca7b96b97840
- VSCode でプロジェクトのフォルダを開いた際、VSCode 画面左下に Python のバージョンが表示される。
  - そこをクリックした際に、以下が選択されていること。（選択されていなければ、手動で選択すること）
    ```
    Python X.X.X 64-bit ('.venv')
    .\venv\Scripts\python.exe
    ```

## デプロイ

依存モジュールが更新されている場合は、以下の手順で requirements.txt を更新する。

**この時、改行コードを LF にすること。そうしないとデプロイに失敗する。**

```
> cd serverless-example-python-vscode
> poetry export -f requirements.txt > requirements.txt
> move ./requirements.txt ./src/
```

ローカルから lambda-layers をデプロイする際は、resources/serverless-lambdalayers.yml 内の以下のコメントアウトを外すこと。
※前提として、Docker for Windows が立ち上がっていることが必要となる。

```
  # dockerizePip: true
```

その上で、各サービスごとに Severless Famework を利用してデプロイを行う。

```
> sls deploy
```

デプロイの基本的な順序は、以下のようになる。

- 必要な AWS リソースの作成（Cognito、DynamoDB など）
  - 必ずしも ServerlessFramework を利用する必要はない。プロジェクトで適切な方法で作成しておくこと。
- Lambda Layers による共通ライブラリのデプロイ
  - src/resources/serverless-lambdalayers.yml
- 各 Lambda の機能のデプロイ

## プロジェクトで利用する際に変更が必要な場所

### ソースコードのディレクトリ設定

- src/{feature01}などのフォルダ名を、プロジェクトの機能名に変更する。
- その内容にあわせて、`.env` ファイル の `PYTHONPATH` の指定内容を更新する。

### custom 配下の環境ごとの内容の設定

- {stage}.yml に環境変数の値など、stage によって変わるものを格納する。
- memory サイズや timeout など、プロジェクトに適した値を設定する。

### serverless.yml （機能ごとに作成）

- service 名
- provider.iam.role.statements の内容
  - 実際に利用するリソースに対してアクセス許可をつける。
- environment
  - 必要な環境変数を定義する。
  - 環境変数がステージによって異なる場合は、値を custom/{stage}.yml で定義する。
- functions
  - Lambda の定義をする。
- その他のリソースを定義する場合は、[serverless framework 公式のドキュメント](https://www.serverless.com/framework/docs/providers/aws/guide/)を参照すること。

### pyproject.toml／requirements.txt

- package は poetry を利用して管理する。
- serverless-layers デプロイのために、requirements.txt を生成すること。
  - `poetry export -f requirements.txt > requirements.txt`

## 開発時の注意点

### AWS Lambda Powertools Python の利用

本内容は、AWS Lambda Powertools Python（以下、Powertools） を適用している。
ログ出力や、トレーシングの内容は、Powertools の内容を参照して実装をすること。

- https://awslabs.github.io/aws-lambda-powertools-python/

### 共通ユーティリティ

Lambda や DynamoDB を利用する上で、よく必要になる処理を、共通ユーティリティとして同梱している。
以下の内容を参照して、活用すること。

- src/common/lambda_util.py
- src/common/timestamp_util.py
- src/tests/lambda_testutils.py （テスト用）

### カバレッジの出力

カバレッジを出力したい場合は、以下のファイルのコメントアウトを外すこと。

- pytest.ini

ただし、この設定を行うと、VSCode でのデバッグ時に、ブレークポイントが効かなくなる問題があるため、適宜切り替えて使うこと。

- https://github.com/microsoft/vscode-python/issues/693
