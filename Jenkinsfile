#!/bin/bash

pipeline {
    agent any

    environment {
        PYTHONPATH = '/Python-3.9.0'
        POETRY_CACHE_DIR = '/tmp/poetry/.cache'
        PYTHONIOENCODING = 'utf-8'
    }

    stages {
        stage('インストール') {
            options {
                timestamps()
            }
            steps {
                script {
                    sh 'python -V'
                    sh 'npm install --no-optional'
                    sh 'poetry install'
                }
            }
        }

        stage('静的解析') {
            options {
                timestamps()
            }
            steps {
                parallel(
                    'Pep8': {
                        dir('.') {
                            script {
                                sh 'poetry run flake8 . --exclude=node_modules/*,tests/*,lib/* --select=E101,E113,E125,E129,E304,E7,F4,F8,N8 --max-line-length=120 || true'
                                recordIssues tool: pep8()
                            }
                        }
                    }
                )
            }
        }

        stage('テスト') {
            environment {
                PYTHONPATH = "src/common:src/accounts:src/feature01:src/feature02:src:${PYTHONPATH}"
            }
            options {
                timestamps()
            }
            steps {
                script {
                    sh 'poetry run pytest'
                }
                // step([
                //     $class: 'JUnitResultArchiver',
                //     testResults: '**/nosetests.xml'
                // ])
                // step([
                //     $class: 'CoberturaPublisher',
                //     coberturaReportFile: '**/coverage.xml',
                //     autoUpdateHealth: false,
                //     autoUpdateStability: false,
                //     failUnhealthy: false,
                //     failUnstable: false,
                //     maxNumberOfBuilds: 0,
                //     onlyStable: false,
                //     zoomCoverageChart: false
                // ])
                // archiveArtifacts '**/nosetests.xml'
            }
        }
        stage('AWS認証設定') {
            steps {
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID', credentialsId: 'serverless-cicd-iac', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
                    script {
                        dir('') {
                            sh 'mkdir -p ~/.aws/'
                            echo "Configure AWS Region."
                            sh 'echo "[default]" > ~/.aws/config'
                            sh 'echo region=us-west-2 >> ~/.aws/config'
                            sh 'echo output=json >> ~/.aws/config'
                            sh 'echo >> ~/.aws/config'
                            sh 'echo "[profile $PROFILE]" >> ~/.aws/config'
                            sh 'echo region=$REGION >> ~/.aws/config'
                            sh 'echo output=json >> ~/.aws/config'

                            echo "Configure AWS Credential."
                            sh 'echo "[default]" > ~/.aws/credentials'
                            sh 'echo aws_access_key_id=DUMMY_ACCESS_KEY_ID >> ~/.aws/credentials'
                            sh 'echo aws_secret_access_key=DUMMY_SECRET_ACCESS_KEY >> ~/.aws/credentials'
                            sh 'echo >> ~/.aws/credentials'
                            sh 'echo "[$PROFILE]" >> ~/.aws/credentials'
                            sh 'echo aws_access_key_id=$AWS_ACCESS_KEY_ID >> ~/.aws/credentials'
                            sh 'echo aws_secret_access_key=$AWS_SECRET_ACCESS_KEY >> ~/.aws/credentials'
                        }
                    }
                }
            }
        }
        stage('Deploy Layer') {
            steps {
                script {
                    dir('') {
                        sh 'chmod +x ./package_update_check.sh'
                        sh 'source ./package_update_check.sh || true'
                }
            }
        }
    }

        stage('デプロイ') {
            steps {
                script {
                    dir('src/account/') {
                        if(DEPLOY_ACCOUNTS.toBoolean()) {
                            // sh 'serverless deploy --stage $STAGE --profile $PROFILE'
                            sh 'echo "deploy accounts."'
                        } else {
                            sh 'echo "Skip to deploy accounts."'
                        }
                    }
                    dir('src/feature01/') {
                        if(DEPLOY_FEATURE01.toBoolean()) {
                            // sh 'serverless deploy --stage $STAGE --profile $PROFILE'
                            sh 'echo "deploy feature01."'
                                                   } else {
                            sh 'echo "Skip to deploy feature01."'
                        }
                     }
                    dir('src/feature02/') {
                        if(DEPLOY_FEATURE02.toBoolean()) {
                            // sh 'serverless deploy --stage $STAGE --profile $PROFILE'
                            sh 'echo "deploy feature01."'
                                                   } else {
                            sh 'echo "Skip to deploy feature02."'
                        }
                     }
                }
            }
        }
    }
}