AWSTemplateFormatVersion: "2010-09-09"
Description: cloud formation template of code pipeline for serverless architecture

Parameters:
  PipelineName:
    Description: A name for pipeline
    Type: String
  S3BucketName:
    Description: The name of the S3 bucket to upload static analysis result
    Type: String
  RepositoryName:
    Default: mirroring-repository-name
    Description: The repository name of the code commit, basically same as gitlab repository
    Type: String
  BranchName:
    Default: develop
    Description: The trigger branch name of gitlab repository.
    Type: String
  CodeBuildCIName:
    Default: code-build-ci-name
    Description: The name of code build for ci.
    Type: String
  CodeBuildCDName:
    Default: code-build-cd-name
    Description: The name of code build for cd.
    Type: String
  DeployStage:
    Default: dev
    Description: deploy stage name.
    Type: String
  ProfileName:
    Default: cross-account-deploy-prod
    Description: deploy stage name.
    Type: String
  DefaultRegion:
    Default: us-west-2
    Description: deploy region.
    Type: String
  RoleArn:
    Default: arn:aws:iam::xxxxxxxxx
    Description: deploy account's role arn.
    Type: String


Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: "CodePipeline Settings"
        Parameters:
          - PipelineName
          - S3BucketName
          - Email
          - RepositoryName
          - BranchName
          - CodeBuildCIName
          - CodeBuildCDName
          - DeployStage
          - ProfileName
          - DefaultRegion
          - RoleArn
Resources:
  ArtifactStoreBucket:
    Type: AWS::S3::Bucket
    Properties:
      VersioningConfiguration:
        Status: Enabled

  StaticAnalysisBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref S3BucketName
      PublicAccessBlockConfiguration:
        BlockPublicAcls: true
        BlockPublicPolicy: true
        IgnorePublicAcls: true
        RestrictPublicBuckets: true

  CodeCommitRepo:
    Type: AWS::CodeCommit::Repository
    Properties:
      RepositoryName: !Ref 'RepositoryName'
      RepositoryDescription: This is a repository for my project with code from MySourceCodeBucket.


  CodeBuildCI:
    Type: AWS::CodeBuild::Project
    Properties: 
      Artifacts: 
        Type: CODEPIPELINE
      BadgeEnabled: False
      Description: CI用のCodeBuild
      Environment: 
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_SMALL
        Image: aws/codebuild/amazonlinux2-x86_64-standard:3.0
        EnvironmentVariables:
          - Name: AWS_BUCKET_NAME
            Type: PLAINTEXT
            Value: !Ref S3BucketName
      Name: !Ref 'CodeBuildCIName'
      ServiceRole: !GetAtt CodeBuildCIRole.Arn
      Source:
        BuildSpec: cicd/aws-deploy/buildspec-ci.yml
        Type: CODEPIPELINE

  CodeBuildCD:
    Type: AWS::CodeBuild::Project
    Properties: 
      Artifacts: 
        Type: CODEPIPELINE
      BadgeEnabled: False
      Description: CD用のCodeBuild
      Environment:
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_SMALL
        Image: aws/codebuild/amazonlinux2-x86_64-standard:3.0
        EnvironmentVariables:
          - Name: STAGE
            Type: PLAINTEXT
            Value: !Ref DeployStage
          - Name: PROFILE_NAME
            Type: PLAINTEXT
            Value: !Ref ProfileName
          - Name: DEFAULT_REGION
            Type: PLAINTEXT
            Value: !Ref DefaultRegion
          - Name: ROLE_ARN
            Type: PLAINTEXT
            Value: !Ref RoleArn

      Name: !Ref 'CodeBuildCDName'
      ServiceRole: !GetAtt CodeBuildCDRole.Arn
      Source:
        BuildSpec: cicd/aws-deploy/buildspec-cd.yml
        Type: CODEPIPELINE


  Pipeline:
    Type: AWS::CodePipeline::Pipeline
    Properties:
      ArtifactStore:
        Location: !Ref 'ArtifactStoreBucket'
        Type: S3
      DisableInboundStageTransitions: []
      Name: !Ref 'PipelineName'
      RoleArn: !GetAtt [PipelineRole, Arn]
      Stages:
        - Name: Source
          Actions:
            - Name: SourceAction
              ActionTypeId:
                Category: Source
                Owner: AWS
                Provider: CodeCommit
                Version: '1'
              Configuration:
                RepositoryName: !Ref 'RepositoryName'
                BranchName: !Ref 'BranchName'
              OutputArtifacts:
                - Name: OutputSource
              RunOrder: '1'
        - Name: Build
          Actions:
            - Name: !Ref 'CodeBuildCIName'
              ActionTypeId:
                Category: Test
                Owner: AWS
                Provider: CodeBuild
                Version: '1'
              InputArtifacts:
                - Name: OutputSource
              Configuration:
                BatchEnabled: 'false'
                CombineArtifacts: 'false'
                ProjectName: !Ref 'CodeBuildCIName'
                PrimarySource: OutputSource
              RunOrder: '1'
        - Name: Deploy
          Actions:
            - Name: !Ref 'CodeBuildCDName'
              ActionTypeId:
                Category: Build
                Owner: AWS
                Provider: CodeBuild
                Version: '1'
              InputArtifacts:
                - Name: OutputSource
              Configuration:
                BatchEnabled: 'false'
                CombineArtifacts: 'false'
                ProjectName: !Ref 'CodeBuildCDName'
                PrimarySource: OutputSource
              RunOrder: '1'

  CFNRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Action: ['sts:AssumeRole']
          Effect: Allow
          Principal:
            Service: [cloudformation.amazonaws.com]
        Version: '2012-10-17'
      Path: /
      Policies:
        - PolicyName: CloudFormationRole
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Action:
                  - 'ec2:*'
                Effect: Allow
                Resource: '*'

  PipelineRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Action: ['sts:AssumeRole']
          Effect: Allow
          Principal:
            Service: [codepipeline.amazonaws.com]
        Version: '2012-10-17'
      Path: /
      Policies:
        - PolicyName: CodePipelineAccess
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Action:
                  - 's3:*'
                  - 'codecommit:*'
                  - 'codebuild:*'
                  - 'cloudformation:CreateStack'
                  - 'cloudformation:DescribeStackResource'
                  - 'cloudformation:DescribeStackEvents'
                  - 'cloudformation:DescribeStackResource'
                  - 'cloudformation:ValidateTemplate'
                  - 'cloudformation:DescribeStacks'
                  - 'cloudformation:DeleteStack'
                  - 'cloudformation:UpdateStack'
                  - 'cloudformation:CreateChangeSet'
                  - 'cloudformation:ExecuteChangeSet'
                  - 'cloudformation:DeleteChangeSet'
                  - 'cloudformation:DescribeChangeSet'
                  - 'cloudformation:SetStackPolicy'
                  - 'iam:PassRole'
                  - 'iam:GetRole'
                  - 'sns:Publish'
                Effect: Allow
                Resource: '*'

  CodeBuildCIRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Action: ['sts:AssumeRole']
          Effect: Allow
          Principal:
            Service: [codebuild.amazonaws.com]
        Version: '2012-10-17'
      Path: /
      Policies:
        - PolicyName: CodeBuildAccess
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Action:
                - 'logs:*'
                - 'lambda:*'
                - 'apigateway:*'
                - 's3:*'
                - 'codebuild:*'
                - 'cloudformation:CreateStack'
                - 'cloudformation:DescribeStacks'
                - 'cloudformation:DescribeStackEvents'
                - 'cloudformation:DescribeStackResource'
                - 'cloudformation:ValidateTemplate'
                - 'cloudformation:DeleteStack'
                - 'cloudformation:UpdateStack'
                - 'cloudformation:CreateChangeSet'
                - 'cloudformation:ExecuteChangeSet'
                - 'cloudformation:DeleteChangeSet'
                - 'cloudformation:DescribeChangeSet'
                - 'cloudformation:SetStackPolicy'
                - 'cloudformation:ListStackResources'
                - 'iam:GetRole'
                - 'iam:CreateRole'
                - 'iam:PassRole'
                - 'iam:PutRolePolicy'
                Effect: Allow
                Resource: '*'

  CodeBuildCDRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Action: ['sts:AssumeRole']
          Effect: Allow
          Principal:
            Service: [codebuild.amazonaws.com]
        Version: '2012-10-17'
      Path: /
      Policies:
        - PolicyName: CodeBuildAccess
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Action:
                - 'logs:*'
                - 'lambda:*'
                - 'apigateway:*'
                - 's3:*'
                - 'codebuild:*'
                - 'cloudformation:CreateStack'
                - 'cloudformation:DescribeStacks'
                - 'cloudformation:DescribeStackEvents'
                - 'cloudformation:DescribeStackResource'
                - 'cloudformation:ValidateTemplate'
                - 'cloudformation:DeleteStack'
                - 'cloudformation:UpdateStack'
                - 'cloudformation:CreateChangeSet'
                - 'cloudformation:ExecuteChangeSet'
                - 'cloudformation:DeleteChangeSet'
                - 'cloudformation:DescribeChangeSet'
                - 'cloudformation:SetStackPolicy'
                - 'cloudformation:ListStackResources'
                - 'iam:GetRole'
                - 'iam:CreateRole'
                - 'iam:PassRole'
                - 'iam:PutRolePolicy'
                - 'sts:AssumeRole'
                Effect: Allow
                Resource: 'arn:aws:iam::216528463001:role/CrossAccountDeploy' # 本番環境のデプロイロールを指定
