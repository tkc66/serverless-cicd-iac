# import json
# import os
# import unittest
# import uuid

# import boto3
# import tests.lambda_testutils as lambda_testutils
# from moto import mock_cognitoidp

# import auth_handler as test_target

# AWS_REGION = os.environ.get('AWS_DEFAULT_REGION', 'us-west-2')


# @mock_cognitoidp
# class AuthHandlerTest(unittest.TestCase):
#     """
#     Test cases of accounts.auth_handler.py
#     """

#     def setUp(self):
#         self.__users = [
#             {'email': 'test_user01@example.com', 'password': 'TestPassword01!', 'name': 'test_user01'},
#             {'email': 'test_user02@example.com', 'password': 'TestPassword02!', 'name': 'test_user02'}
#         ]

#         cognito_idp = boto3.client('cognito-idp', AWS_REGION)

#         self.__pool_id = cognito_idp.create_user_pool(
#             PoolName='test_user_pool',
#             DeviceConfiguration={'ChallengeRequiredOnNewDevice': False}
#         ).get('UserPool').get('Id')

#         self.__pool_client_id = cognito_idp.create_user_pool_client(
#             ClientName='test_user_pool_client',
#             UserPoolId=self.__pool_id
#         ).get('UserPoolClient').get('ClientId')

#         for user in self.__users:
#             temppass = str(uuid.uuid4())

#             cognito_idp.admin_create_user(
#                 UserPoolId=self.__pool_id,
#                 Username=user.get('email'),
#                 TemporaryPassword=temppass,
#                 UserAttributes=[
#                     {'Name': 'name', 'Value': user.get('name')}
#                 ]
#             )

#             # AuthFlow は、ADMIN_USER_PASSWORD_AUTH だとmoto自体が対応していないため、
#             # ADMIN_NO_SRP_AUTH を利用する。
#             result_auth = cognito_idp.admin_initiate_auth(
#                 UserPoolId=self.__pool_id,
#                 ClientId=self.__pool_client_id,
#                 AuthFlow='ADMIN_NO_SRP_AUTH',
#                 AuthParameters={'USERNAME': user.get('email'), 'PASSWORD': temppass}
#             )

#             cognito_idp.respond_to_auth_challenge(
#                 Session=result_auth['Session'],
#                 ClientId=self.__pool_client_id,
#                 ChallengeName='NEW_PASSWORD_REQUIRED',
#                 ChallengeResponses={'USERNAME': user.get('email'), 'NEW_PASSWORD': user.get('password')}
#             )

#         # set environment
#         test_target.COGNITO_USER_POOL_ID = self.__pool_id
#         test_target.COGNITO_CLIENT_ID = self.__pool_client_id
#         test_target.COGNITO_AUTH_FLOW = 'ADMIN_NO_SRP_AUTH'

#     def tearDown(self):
#         pass

#     def test_auth_handler__success(self):
#         """
#         正常ログイン
#         """

#         # Prepare
#         body_param = {
#             'email': self.__users[0].get('email'),
#             'password': self.__users[0].get('password')
#         }
#         event = lambda_testutils.create_event(body=body_param)
#         context = lambda_testutils.create_context()

#         # Execute
#         response = test_target.authenticate_handler(event, context)
#         response_status = response['statusCode']
#         response_body = json.loads(response['body'])

#         # Assert
#         self.assertEqual(response_status, '200')
#         self.assertIsNotNone(response_body['token'])

#         # JWT token は "." を2つ持つ
#         token = response_body['token']
#         self.assertEqual(token.count('.'), 2)

#     def test_auth_handler__invalid_user(self):
#         """
#         存在しないユーザー
#         """

#         # Prepare
#         body_param = {
#             'email': 'dummy_user',
#             'password': 'dummy_pass'
#         }
#         event = lambda_testutils.create_event(body=body_param)
#         context = lambda_testutils.create_context()

#         # Execute
#         response = test_target.authenticate_handler(event, context)
#         response_status = response['statusCode']

#         # Assert
#         self.assertEqual(response_status, '401')

#     def test_auth_handler__invalid_password(self):
#         """
#         パスワード不正
#         """

#         # Prepare
#         body_param = {
#             'email': self.__users[0].get('email'),
#             'password': 'dummy'
#         }
#         event = lambda_testutils.create_event(body=body_param)
#         context = lambda_testutils.create_context()

#         # Execute
#         response = test_target.authenticate_handler(event, context)
#         response_status = response['statusCode']

#         # Assert
#         self.assertEqual(response_status, '401')
