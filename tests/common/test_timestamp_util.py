import unittest
from datetime import datetime, timezone, timedelta

from common import timestamp_util


class TestTimestampUtil(unittest.TestCase):

    def test_format_timestamp__dict(self):
        now = datetime.now()
        now_utc = now.astimezone(tz=timezone.utc)

        param = {
            'date': now.date(),
            'time': now.time(),
            'datetime': now,
            'str': 'datetime format'
        }

        actual = timestamp_util.format_timestamp(param)

        # datetime の場合はUTCとなる
        self.assertEqual(actual['date'], now.strftime('%Y-%m-%d'))
        self.assertEqual(actual['time'], now.strftime('%H:%M:%S.%f')[:-3])
        self.assertEqual(actual['datetime'], now_utc.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z')
        self.assertEqual(actual['str'], 'datetime format')

    def test_format_timestamp__list(self):
        dt_str = '2021-01-02T03:04:05.123'
        dt = datetime.fromisoformat(dt_str)
        dt_utc = dt.astimezone(tz=timezone.utc)

        actual = {
            'date': [],
            'time': [],
            'datetime': [],
            'str': []
        }

        tmp_dt_utc = dt_utc
        for index in range(3):
            tmp_dt_utc = tmp_dt_utc + timedelta(days=-1, hours=8)
            actual['date'].append(tmp_dt_utc.strftime('%Y-%m-%d'))
            actual['time'].append(tmp_dt_utc.strftime('%H:%M:%S.%f')[:-3])
            actual['datetime'].append(tmp_dt_utc.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z')
            actual['str'].append('datetime format:' + str(index))

        # 元の値がUTCのため、UTCの日時となる。
        # 計算を踏まえて、最後の要素だけを確認。
        self.assertEqual(actual['date'][2], '2020-12-31')
        self.assertEqual(actual['time'][2], '03:04:05.123')
        self.assertEqual(actual['datetime'][2], '2020-12-31T03:04:05.123Z')
        self.assertEqual(actual['str'][2], 'datetime format:2')

    def test_timestamp_fromformat__iso_seconds(self):
        expected, actual = _timestamp_fromiso('2021-01-02T03:04:05')
        self.assertEqual(actual, expected)

    def test_timestamp_fromformat__iso_milliseconds(self):
        expected, actual = _timestamp_fromiso('2021-01-02T03:04:05.123')
        self.assertEqual(actual, expected)

    def test_timestamp_fromformat__iso_microseconds(self):
        expected, actual = _timestamp_fromiso('2021-01-02T03:04:05.123456')
        self.assertEqual(actual, expected)

    def test_timestamp_fromformat__isoZ_seconds(self):
        expected, actual = _timestamp_fromiso_Z('2021-01-02T03:04:05Z')
        self.assertEqual(actual, expected)

    def test_timestamp_fromformat__isoZ_milliseconds(self):
        expected, actual = _timestamp_fromiso_Z('2021-01-02T03:04:05.123Z')
        self.assertEqual(actual, expected)

    def test_timestamp_fromformat__isoZ_microseconds(self):
        expected, actual = _timestamp_fromiso_Z('2021-01-02T03:04:05.123456Z')
        self.assertEqual(actual, expected)

    def test_timestamp_fromformat__iso_utc(self):
        expected, actual = _timestamp_fromiso('2021-01-02T03:04:05.123+00:00')
        self.assertEqual(actual, expected)

    def test_timestamp_fromformat__iso_jst(self):
        expected, actual = _timestamp_fromiso('2021-01-02T03:04:05.123+09:00')
        self.assertEqual(actual, expected)

    def test_asutc_jst(self):
        JST = timezone(timedelta(hours=+9))

        now = datetime.now()
        now_jst = now.astimezone(JST)

        now_utc = timestamp_util.asutc(now_jst)

        # タイムゾーンはUTC、時差9時間
        self.assertEqual(now_utc.tzinfo, timezone.utc)
        self.assertEqual(((now_jst.hour - now_utc.hour) + 24) % 24, 9)

    def test_asutc_utc(self):
        now = datetime.now()
        now_utc1 = now.astimezone(timezone.utc)
        now_utc2 = timestamp_util.asutc(now_utc1)

        # タイムゾーンはUTC、時差0時間
        self.assertEqual(now_utc2.tzinfo, timezone.utc)
        self.assertEqual(now_utc1.day, now_utc2.day)
        self.assertEqual(now_utc1.hour, now_utc2.hour)


# ----- Test Utils


def _timestamp_fromiso(str):
    expected = datetime.fromisoformat(str)
    actual = timestamp_util.timestamp_from(str)
    return expected, actual


def _timestamp_fromiso_Z(str):
    if str[-1:] == 'Z':
        iso_str = str.replace('Z', '+00:00')
    else:
        iso_str = str

    expected = datetime.fromisoformat(iso_str)
    actual = timestamp_util.timestamp_from(str)
    return expected, actual
