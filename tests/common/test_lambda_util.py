# import decimal
# import unittest
# from dataclasses import dataclass, field
# from datetime import date, datetime, time, timezone
# from typing import List

# from common import lambda_util


# class TestLambdaUtil(unittest.TestCase):

#     def test_convert_to_primitive__dict(self):
#         """
#         dict形式が保持するパラメータをプリミティブ型のパラメータに変換する。
#         """
#         now = datetime.now(tz=timezone.utc)

#         dict_data = {
#             'int_param': int(1),
#             'float_param': float(1.0),
#             'str_param': 'test',
#             'datetime_param': now,
#             'date_param': now.date(),
#             'time_param': now.time(),
#             'none_param': None,
#         }

#         actual = lambda_util.convert_to_primitive(obj=dict_data)

#         self.assertEqual(actual['int_param'], 1)
#         self.assertEqual(actual['float_param'], 1.0)
#         self.assertEqual(actual['str_param'], 'test')
#         self.assertEqual(actual['datetime_param'], now.isoformat()[:-9] + 'Z')
#         self.assertEqual(actual['date_param'], now.date().isoformat())
#         self.assertEqual(actual['time_param'], now.time().isoformat()[:-3])
#         self.assertEqual(actual['none_param'], None)

#     def test_convert_to_primitive__entity(self):
#         """
#         dataclassのオブジェクトをプリミティブ型のパラメータを保持するdict形式に変換する。
#         """
#         test_parent = TestParentEntity()
#         test_children = test_parent.children
#         datetime_param = test_parent.datetime_param

#         actual = lambda_util.convert_to_primitive(obj=test_parent)

#         # parent
#         self.assertEqual(actual['int_param'], 1)
#         self.assertEqual(actual['float_param'], 1.0)
#         self.assertEqual(actual['str_param'], 'test')
#         self.assertEqual(actual['datetime_param'], datetime_param.isoformat()[:-9] + 'Z')
#         self.assertEqual(actual['date_param'], datetime_param.date().isoformat())
#         self.assertEqual(actual['time_param'], datetime_param.time().isoformat()[:-3])
#         self.assertEqual(actual['none_param'], None)

#         # child:0
#         actual_children = actual['children']
#         self.assertEqual(len(actual_children), 3)
#         self.assertEqual(actual_children[0]['str_param'], 'index:0')
#         self.assertEqual(actual_children[0]['datetime_param'], test_children[0].datetime_param.isoformat()[:-9] + 'Z')

#     def test_convert_to_persistence__dict(self):
#         """
#         dict形式が保持するパラメータをDynamoDB用に永続化するためのパラメータに変換する。
#         """
#         now = datetime.now(tz=timezone.utc)

#         dict_data = {
#             'int_param': int(1),
#             'float_param': float(1.0),
#             'str_param': 'test',
#             'datetime_param': now,
#             'date_param': now.date(),
#             'time_param': now.time(),
#             'none_param': None,
#         }

#         actual = lambda_util.convert_to_persistence(obj=dict_data)

#         self.assertEqual(actual['int_param'], decimal.Decimal(str(1)))
#         self.assertEqual(actual['float_param'], decimal.Decimal(str(1.0)))
#         self.assertEqual(actual['str_param'], 'test')
#         self.assertEqual(actual['datetime_param'], now.isoformat()[:-9] + 'Z')
#         self.assertEqual(actual['date_param'], now.date().isoformat())
#         self.assertEqual(actual['time_param'], now.time().isoformat()[:-3])
#         self.assertEqual(actual['none_param'], None)

#     def test_convert_to_persistence__entity(self):
#         """
#         dataclassのオブジェクトをDynamoDB用に永続化するためのパラメータに変換する。
#         """
#         test_parent = TestParentEntity()
#         test_children = test_parent.children
#         datetime_param = test_parent.datetime_param

#         actual = lambda_util.convert_to_persistence(obj=test_parent)

#         # parent
#         self.assertEqual(actual['int_param'], decimal.Decimal(str(1)))
#         self.assertEqual(actual['float_param'], decimal.Decimal(str(1.0)))
#         self.assertEqual(actual['str_param'], 'test')
#         self.assertEqual(actual['datetime_param'], datetime_param.isoformat()[:-9] + 'Z')
#         self.assertEqual(actual['date_param'], datetime_param.date().isoformat())
#         self.assertEqual(actual['time_param'], datetime_param.time().isoformat()[:-3])
#         self.assertEqual(actual['none_param'], None)

#         # child:0
#         actual_children = actual['children']
#         self.assertEqual(len(actual_children), 3)
#         self.assertEqual(actual_children[0]['str_param'], 'index:0')
#         self.assertEqual(actual_children[0]['datetime_param'], test_children[0].datetime_param.isoformat()[:-9] + 'Z')

#     def test_domainentity_to_dict__normal(self):
#         """
#         dataclassのオブジェクトをそのままdict形式に変換する（各パラメータの型はそのまま保持する）。
#         """
#         test_parent = TestParentEntity()
#         actual = test_parent.to_dict()

#         self.assertEqual(type(actual['datetime_param']), datetime)
#         self.assertEqual(type(actual['date_param']), date)
#         self.assertEqual(type(actual['time_param']), time)
#         self.assertEqual(type(actual['children']), list)
#         self.assertEqual(type(actual['children'][0]), dict)
#         self.assertEqual(actual['none_param'], None)

#     def test_domainentity_to_dict__primitive(self):
#         """
#         dataclassのオブジェクトをプリミティブ型のパラメータを保持するdict形式に変換する。
#         """
#         test_parent = TestParentEntity()
#         test_children = test_parent.children
#         datetime_param = test_parent.datetime_param

#         actual = test_parent.to_dict(as_primitive=True)

#         # parent
#         self.assertEqual(actual['int_param'], 1)
#         self.assertEqual(actual['float_param'], 1.0)
#         self.assertEqual(actual['str_param'], 'test')
#         self.assertEqual(actual['datetime_param'], datetime_param.isoformat()[:-9] + 'Z')
#         self.assertEqual(actual['date_param'], datetime_param.date().isoformat())
#         self.assertEqual(actual['time_param'], datetime_param.time().isoformat()[:-3])
#         self.assertEqual(actual['none_param'], None)

#         # child:0
#         actual_children = actual['children']
#         self.assertEqual(len(actual_children), 3)
#         self.assertEqual(actual_children[0]['str_param'], 'index:0')
#         self.assertEqual(actual_children[0]['datetime_param'], test_children[0].datetime_param.isoformat()[:-9] + 'Z')

#     def test_domainentity_to_persistence__normal(self):
#         """
#         dataclassのオブジェクトをDynamoDB用に永続化するためのパラメータに変換する。
#         """
#         test_parent = TestParentEntity()
#         test_children = test_parent.children
#         datetime_param = test_parent.datetime_param

#         actual = test_parent.to_persistence()

#         # parent
#         self.assertEqual(actual['int_param'], decimal.Decimal(str(1)))
#         self.assertEqual(actual['float_param'], decimal.Decimal(str(1.0)))
#         self.assertEqual(actual['str_param'], 'test')
#         self.assertEqual(actual['datetime_param'], datetime_param.isoformat()[:-9] + 'Z')
#         self.assertEqual(actual['date_param'], datetime_param.date().isoformat())
#         self.assertEqual(actual['time_param'], datetime_param.time().isoformat()[:-3])
#         self.assertEqual(actual['none_param'], None)

#         # child:0
#         actual_children = actual['children']
#         self.assertEqual(len(actual_children), 3)
#         self.assertEqual(actual_children[0]['str_param'], 'index:0')
#         self.assertEqual(actual_children[0]['datetime_param'], test_children[0].datetime_param.isoformat()[:-9] + 'Z')


# # ----- Test Utils


# @dataclass
# class TestChildEntity(lambda_util.DomainEntity):
#     str_param: str = None
#     datetime_param: datetime = datetime.now(tz=timezone.utc)


# @dataclass
# class TestParentEntity(lambda_util.DomainEntity):
#     int_param: int = 1
#     float_param: float = 1.0
#     str_param: str = 'test'
#     datetime_param: datetime = None
#     date_param: date = None
#     time_param: time = None
#     none_param: str = None
#     children: List[TestChildEntity] = field(default_factory=list)

#     def __post_init__(self):
#         now = datetime.now(tz=timezone.utc)
#         self.datetime_param = now
#         self.date_param = now.date()
#         self.time_param = now.time()

#         for index in range(3):
#             self.children.append(TestChildEntity(str_param='index:' + str(index)))
