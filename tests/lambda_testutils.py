import json
import os
import uuid
from collections import namedtuple

AWS_REGION = os.environ.get('AWS_DEFAULT_REGION', 'us-west-2')


def create_event(path_parameters=None, query_parameters=None, body=None, auth=None):
    event = {}
    if path_parameters:
        event['pathParameters'] = path_parameters
    if query_parameters:
        event['queryStringParameters'] = query_parameters
    if body:
        event['body'] = json.dumps(body)
    if auth:
        event['requestContext'] = {
            'authorizer': {
                'claims': {
                    'email': auth.email
                }
            }
        }
    return event


def create_context(function_name='test_handler'):
    lambda_context = {
        'function_name': function_name,
        'memory_limit_in_mb': 128,
        'invoked_function_arn': 'arn:aws:lambda:' + AWS_REGION + ':123456789012:' + function_name,
        'aws_request_id': str(uuid.uuid4())
    }
    context = namedtuple('LambdaContext', lambda_context.keys())(*lambda_context.values())
    return context
