import uuid

import boto3
from botocore.exceptions import ClientError
from common.timestamp_util import format_timestamp, utcnow
from moto import mock_dynamodb2


@mock_dynamodb2
class TodoTestSetup():

    def __init__(self):
        # テストデータ
        todo_list = []
        for index in range(3):
            id = str(uuid.uuid4())
            todo = {
                'todo_id': id,
                'todo': 'test todo #' + str(index),
                'updated_timestamp': format_timestamp(utcnow())
            }
            todo_list.append(todo)

        self.todo_list = todo_list

        self.__dynamodb = boto3.resource('dynamodb')

    def setUpAll(self):
        self.create_dynamodb_table()
        self.setup_dynamodb_data()

    def cleanAll(self):
        self.delete_dynamodb_table()

    def create_dynamodb_table(self):
        self.__dynamodb.create_table(
            TableName='todo',
            AttributeDefinitions=[
                {'AttributeName': 'todo_id', 'AttributeType': 'S'},
            ],
            KeySchema=[
                {'AttributeName': 'todo_id', 'KeyType': 'HASH'}
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )

    def delete_dynamodb_table(self):
        # ResourceNotFoundException が発生する場合はテーブルが存在しない
        try:
            self.__dynamodb.Table('todo').delete()
        except ClientError as err:
            if err.response['Error']['Code'] == 'ResourceNotFoundException':
                pass
            else:
                raise err

    def setup_dynamodb_data(self):
        todo_table = self.__dynamodb.Table('todo')

        for todo in self.todo_list:
            todo_table.put_item(Item=todo)
