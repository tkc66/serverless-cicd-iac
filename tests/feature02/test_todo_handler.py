# import json
# import unittest

# from moto import mock_dynamodb2
# from tests import lambda_testutils

# from feature02 import todo_handler

# from .todo_test_setup import TodoTestSetup


# @mock_dynamodb2
# class TestTodoHandler(unittest.TestCase):

#     __test_setup = TodoTestSetup()

#     def setUp(self):
#         self.__test_setup.setUpAll()

#     def tearDown(self):
#         self.__test_setup.cleanAll()

#     def test_find_todo_list_handler_normal(self):
#         """
#         TODOを取得する。

#         Conditions:
#             DynamoDBにTODOが存在する。

#         Expected:
#             TODOが3件取得される。
#         """
#         # Prepare
#         event = lambda_testutils.create_event()
#         context = lambda_testutils.create_context()

#         # Execute
#         actual_response = todo_handler.find_todo_list_handler(event, context)
#         actual_body = json.loads(actual_response['body'])

#         # Assert
#         self.assertEqual('200', actual_response['statusCode'])
#         self.assertEqual(len(actual_body), 3)

#     def test_find_todo_handler__normal(self):
#         """
#         TODOを取得する。

#         Conditions:
#             DynamoDBにTODOが存在する。

#         Expected:
#             TODOが取得される。
#         """

#         # Prepare
#         todo = self.__test_setup.todo_list[0]
#         event = lambda_testutils.create_event(path_parameters={'todo_id': todo['todo_id']})
#         context = lambda_testutils.create_context()

#         # Execute
#         actual_response = todo_handler.find_todo_handler(event, context)
#         actual_body = json.loads(actual_response['body'])

#         # Assert
#         self.assertEqual('200', actual_response['statusCode'])
#         self.assertEqual(todo['todo_id'], actual_body['todo_id'])
#         self.assertIsNotNone(actual_body['todo'])
#         self.assertIsNotNone(actual_body['updated_timestamp'])

#     def test_add_todo_handler__normal(self):
#         """
#         TODOを追加する。

#         Conditions:
#             1件のTODO

#         Expected:
#             TODOが登録され、正常応答が変える。
#         """

#         # Prepare
#         event = lambda_testutils.create_event(body={'todo': 'test todo'})
#         context = lambda_testutils.create_context()

#         # Execute
#         actual_response = todo_handler.add_todo_handler(event, context)
#         actual_body = json.loads(actual_response['body'])

#         # Assert
#         self.assertEqual('200', actual_response['statusCode'])
#         self.assertEqual('test todo', actual_body['todo'])
#         self.assertIsNotNone(actual_body['todo_id'])
#         self.assertIsNotNone(actual_body['updated_timestamp'])

#     def test_update_todo_handler__normal(self):
#         """
#         TODOを更新する。

#         Conditions:
#             1件のTODO

#         Expected:
#             TODOが更新され、正常応答が変える。
#         """

#         # Prepare
#         todo = self.__test_setup.todo_list[0]
#         todo_updated = todo.copy()
#         todo_updated['todo'] = 'test updated'
#         event = lambda_testutils.create_event(body=todo_updated)
#         context = lambda_testutils.create_context()

#         # Execute
#         actual_response = todo_handler.update_todo_handler(event, context)
#         actual_body = json.loads(actual_response['body'])

#         # Assert
#         self.assertEqual('200', actual_response['statusCode'])
#         self.assertEqual(todo_updated['todo_id'], actual_body['todo_id'])
#         self.assertEqual(todo_updated['todo'], actual_body['todo'])
#         self.assertLess(todo_updated['updated_timestamp'], actual_body['updated_timestamp'])

#     def test_delete_todo_handler__normal(self):
#         """
#         TODOを削除する。

#         Conditions:
#             DynamoDBにTODOが存在する。

#         Expected:
#             TODOが削除される（戻り値はTrue）。
#         """

#         # Prepare
#         todo = self.__test_setup.todo_list[0]
#         event = lambda_testutils.create_event(path_parameters={'todo_id': todo['todo_id']})
#         context = lambda_testutils.create_context()

#         # Execute
#         actual_response = todo_handler.delete_todo_handler(event, context)
#         actual_body = json.loads(actual_response['body'])

#         # Assert
#         self.assertEqual('200', actual_response['statusCode'])
#         self.assertEqual(True, actual_body)

#     def test_delete_todo_handler__none(self):
#         """
#         TODOを削除する（削除対象なし）。

#         Conditions:
#             DynamoDBにTODOが存在しない。

#         Expected:
#             処理は成功し、例外は発生しない（戻り値はFalse）。
#         """

#         # Prepare
#         event = lambda_testutils.create_event(path_parameters={'todo_id': 'dummuy'})
#         context = lambda_testutils.create_context()

#         # Execute
#         actual_response = todo_handler.delete_todo_handler(event, context)
#         actual_body = json.loads(actual_response['body'])

#         # Assert
#         self.assertEqual('200', actual_response['statusCode'])
#         self.assertEqual(False, actual_body)
